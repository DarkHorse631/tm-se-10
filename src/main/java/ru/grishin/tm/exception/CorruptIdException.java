package ru.grishin.tm.exception;

public final class CorruptIdException extends AbstractException {
    public CorruptIdException() {
        super("Id is corrupted!");
    }
}
