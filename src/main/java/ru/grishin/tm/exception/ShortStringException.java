package ru.grishin.tm.exception;

public final class ShortStringException extends AbstractException {
    public ShortStringException() {
        super("Too short string!");
    }
}
