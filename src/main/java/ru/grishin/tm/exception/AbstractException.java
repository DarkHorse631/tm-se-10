package ru.grishin.tm.exception;

public abstract class AbstractException extends Exception {
    public AbstractException(String message) {
        super(message);
    }
}
