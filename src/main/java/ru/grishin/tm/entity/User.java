package ru.grishin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "user")
public final class User extends AbstractEntity implements Serializable {

    @Nullable
    private String login;
    @Nullable
    private String password;
    @Nullable
    private RoleType roleType;

    @Override
    public String toString() {
        return "User id:[" + id +
                "] user login: [" + login +
                "] password: [" + password +
                "] roleType: [" + roleType + "]";
    }
}
