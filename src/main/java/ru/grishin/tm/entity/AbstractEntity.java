package ru.grishin.tm.entity;

import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

public abstract class AbstractEntity implements Serializable {

    @Nullable
    protected String id;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }
}
