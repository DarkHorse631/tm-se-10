package ru.grishin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.grishin.tm.api.service.TerminalService;
import ru.grishin.tm.command.AbstractCommand;

import java.util.*;

public final class TerminalServiceImpl implements TerminalService {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.grishin.tm.command").getSubTypesOf(AbstractCommand.class);

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @Override
    public void addCommand(@NotNull final String key, @NotNull final AbstractCommand command) {
        commands.put(key, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@NotNull final String key) {
        return commands.get(key);
    }

    @Nullable
    @Override
    public Set<Class<? extends AbstractCommand>> getClasses() {
        return classes;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }
}
