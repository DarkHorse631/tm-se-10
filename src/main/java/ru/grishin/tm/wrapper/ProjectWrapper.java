package ru.grishin.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.Project;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "projectWrapper")
@XmlAccessorType(XmlAccessType.FIELD)
public final class ProjectWrapper {

    @Nullable
    @XmlElement(name = "projectList")
    private List<Project> projectList = null;

    @Nullable
    public List<Project> getProjectList() {
        return projectList;
    }


    public void setProjectList(@Nullable final List<Project> projectList) {
        this.projectList = projectList;
    }
}
