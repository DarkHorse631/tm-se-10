package ru.grishin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull
    ProjectService getProjectService();

    @NotNull
    TaskService getTaskService();

    @NotNull
    UserService getUserService();

    @NotNull
    TerminalService getTerminalService();
}
