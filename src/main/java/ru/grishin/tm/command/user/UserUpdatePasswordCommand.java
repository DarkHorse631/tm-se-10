package ru.grishin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class UserUpdatePasswordCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "uup";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update password to current user.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Update current password--");
        System.out.print("Enter current password: ");
        final String currentPassword = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new password: ");
        final String pass1 = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Confirm your password: ");
        final String pass2 = serviceLocator.getTerminalService().getScanner().nextLine();
        if (pass1.equals(pass2)) {
            serviceLocator.getUserService().updatePassword(serviceLocator.getUserService().getCurrentUser().getId(), currentPassword, pass1);
            System.out.println("[PASSWORD UPDATED]");
        } else {
            System.out.println("Passwords don't match!");
            execute();
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
