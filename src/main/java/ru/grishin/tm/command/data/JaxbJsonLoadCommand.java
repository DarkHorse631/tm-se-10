package ru.grishin.tm.command.data;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.wrapper.ProjectWrapper;
import ru.grishin.tm.wrapper.TaskWrapper;
import ru.grishin.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class JaxbJsonLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "jaxbjsonload";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from Json with JAX-B.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Load data from Json with JAX-B--");
        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @NotNull final Unmarshaller userUnmarshaller = userContext.createUnmarshaller();
        userUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        userUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final UserWrapper userWrapper = (UserWrapper) userUnmarshaller.unmarshal(new File("src/main/file/users.json"));
        for (final User user : userWrapper.getUserList()) serviceLocator.getUserService().merge(user);

        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @NotNull final Unmarshaller projectUnmarshaller = projectContext.createUnmarshaller();
        projectUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        projectUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final ProjectWrapper projectWrapper = (ProjectWrapper) projectUnmarshaller.unmarshal(new File("src/main/file/projects.json"));
        for (final Project project : projectWrapper.getProjectList()) serviceLocator.getProjectService().merge(project);

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @NotNull final Unmarshaller taskUnmarshaller = taskContext.createUnmarshaller();
        taskUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        taskUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @Nullable final TaskWrapper taskWrapper = (TaskWrapper) taskUnmarshaller.unmarshal(new File("src/main/file/tasks.json"));
        for (final Task task : taskWrapper.getTaskList()) serviceLocator.getTaskService().merge(task);
        System.out.println("[LOAD COMPLETED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
