package ru.grishin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.wrapper.ProjectWrapper;
import ru.grishin.tm.wrapper.TaskWrapper;
import ru.grishin.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class JaxbXmlSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "jaxbxmlsave";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save objects to XML with JAX-B.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Save objects to XML with JAX-B--");
        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @NotNull final Marshaller userMarshaller = userContext.createMarshaller();
        @NotNull final UserWrapper userWrapper = new UserWrapper();
        userWrapper.setUserList(serviceLocator.getUserService().findAll());
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        userMarshaller.marshal(userWrapper, new File("src/main/file/users.xml"));

        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @NotNull final Marshaller projectMarshaller = projectContext.createMarshaller();
        @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper();
        projectWrapper.setProjectList(serviceLocator.getProjectService().findAll());
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        projectMarshaller.marshal(projectWrapper, new File("src/main/file/projects.xml"));

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @NotNull final Marshaller taskMarshaller = taskContext.createMarshaller();
        @NotNull final TaskWrapper taskWrapper = new TaskWrapper();
        taskWrapper.setTaskList(serviceLocator.getTaskService().findAll());
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        taskMarshaller.marshal(taskWrapper, new File("src/main/file/tasks.xml"));
        System.out.println("[SAVED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
