package ru.grishin.tm.command.data;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.wrapper.ProjectWrapper;
import ru.grishin.tm.wrapper.TaskWrapper;
import ru.grishin.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class JaxbJsonSaveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "jaxbjsonsave";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to Json with JAX-B.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Save data to Json with JAX-B--");
        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @NotNull final Marshaller userMarshaller = userContext.createMarshaller();
        @NotNull final UserWrapper userWrapper = new UserWrapper();
        userWrapper.setUserList(serviceLocator.getUserService().findAll());
        userMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        userMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        userMarshaller.marshal(userWrapper, new File("src/main/file/users.json"));

        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @NotNull final Marshaller projectMarshaller = projectContext.createMarshaller();
        @NotNull final ProjectWrapper projectWrapper = new ProjectWrapper();
        projectWrapper.setProjectList(serviceLocator.getProjectService().findAll());
        projectMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        projectMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.marshal(projectWrapper, new File("src/main/file/projects.json"));

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @NotNull final Marshaller taskMarshaller = taskContext.createMarshaller();
        @NotNull final TaskWrapper taskWrapper = new TaskWrapper();
        taskWrapper.setTaskList(serviceLocator.getTaskService().findAll());
        taskMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        taskMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        taskMarshaller.marshal(taskWrapper, new File("src/main/file/tasks.json"));
        System.out.println("[SAVED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
