package ru.grishin.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.io.File;
import java.util.Iterator;

public final class JacksonXmlLoadCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "jacksonxmlload";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from XML with Jackson.";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        System.out.println("--Load data from XML with Jackson--");
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        @NotNull final XmlMapper xmlMapper = new XmlMapper(module);
        @NotNull final Iterator<User> userIterator = xmlMapper.readerFor(User.class).readValues(new File("src/main/file/usersJackson.xml"));
        while (userIterator.hasNext()) serviceLocator.getUserService().merge(userIterator.next());

        @NotNull final Iterator<Project> projectIterator = xmlMapper.readerFor(Project.class).readValues(new File("src/main/file/projectsJackson.xml"));
        while (projectIterator.hasNext()) serviceLocator.getProjectService().merge(projectIterator.next());

        @NotNull final Iterator<Task> taskIterator = xmlMapper.readerFor(Task.class).readValues(new File("src/main/file/tasksJackson.xml"));
        while (taskIterator.hasNext()) serviceLocator.getTaskService().merge(taskIterator.next());
        System.out.println("[LOAD COMPLETED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
