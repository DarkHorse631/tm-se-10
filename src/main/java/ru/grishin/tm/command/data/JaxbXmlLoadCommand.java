package ru.grishin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.wrapper.ProjectWrapper;
import ru.grishin.tm.wrapper.TaskWrapper;
import ru.grishin.tm.wrapper.UserWrapper;

import javax.xml.bind.JAXBContext;
import java.io.File;

public final class JaxbXmlLoadCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "jaxbxmlload";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load objects from XML with JAX-B.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Load objects from XML with JAX-B--");
        @NotNull final JAXBContext userContext = JAXBContext.newInstance(UserWrapper.class);
        @Nullable final UserWrapper userWrapper = (UserWrapper) userContext.createUnmarshaller().unmarshal(new File("src/main/file/users.xml"));
        for (final User user : userWrapper.getUserList()) serviceLocator.getUserService().merge(user);

        @NotNull final JAXBContext projectContext = JAXBContext.newInstance(ProjectWrapper.class);
        @Nullable final ProjectWrapper projectWrapper = (ProjectWrapper) projectContext.createUnmarshaller().unmarshal(new File("src/main/file/projects.xml"));
        for (final Project project : projectWrapper.getProjectList()) serviceLocator.getProjectService().merge(project);

        @NotNull final JAXBContext taskContext = JAXBContext.newInstance(TaskWrapper.class);
        @Nullable final TaskWrapper taskWrapper = (TaskWrapper) taskContext.createUnmarshaller().unmarshal(new File("src/main/file/tasks.xml"));
        for (final Task task : taskWrapper.getTaskList()) serviceLocator.getTaskService().merge(task);
        System.out.println("[LOAD COMPLETED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
