package ru.grishin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

public final class DataDeserializableCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "dd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deserializable data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Deserializable data--");
        try (
                @NotNull final FileInputStream userInput = new FileInputStream("src/main/file/users");
                @NotNull final FileInputStream projectInput = new FileInputStream("src/main/file/projects");
                @NotNull final FileInputStream taskInput = new FileInputStream("src/main/file/tasks")
        ) {
            @NotNull final ObjectInputStream userInputStream = new ObjectInputStream(userInput);
            @Nullable final List<User> users = (List<User>) userInputStream.readObject();
            for (User user : users) serviceLocator.getUserService().merge(user);
            @NotNull final ObjectInputStream projectInputStream = new ObjectInputStream(projectInput);
            @Nullable final List<Project> projects = (List<Project>) projectInputStream.readObject();
            for (Project project : projects) serviceLocator.getProjectService().merge(project);
            @NotNull final ObjectInputStream taskInputStream = new ObjectInputStream(taskInput);
            @Nullable final List<Task> tasks = (List<Task>) taskInputStream.readObject();
            for (Task task : tasks) serviceLocator.getTaskService().merge(task);
            System.out.println("[DESERIALIZABLE COMPLETED]");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
