package ru.grishin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public final class DataSerializeCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "ds";
    }

    @Override
    public @NotNull String getDescription() {
        return "Serialize data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Serializing data--");
        @NotNull final FileOutputStream userOutput = new FileOutputStream("src/main/file/users");
        @NotNull final FileOutputStream projectOutput = new FileOutputStream("src/main/file/projects");
        @NotNull final FileOutputStream taskOutput = new FileOutputStream("src/main/file/tasks");
        @Nullable final List<User> users = new LinkedList<>(serviceLocator.getUserService().findAll());
        @Nullable final List<Project> projects = new LinkedList<>(serviceLocator.getProjectService().findAll());
        @Nullable final List<Task> tasks = new LinkedList<>(serviceLocator.getTaskService().findAll());
        try {
            @NotNull final ObjectOutputStream userOutputStream = new ObjectOutputStream(userOutput);
            userOutputStream.writeObject(users);
            @NotNull final ObjectOutputStream projectOutputStream = new ObjectOutputStream(projectOutput);
            projectOutputStream.writeObject(projects);
            @NotNull final ObjectOutputStream taskOutputStream = new ObjectOutputStream(taskOutput);
            taskOutputStream.writeObject(tasks);
            System.out.println("[SERIALIZATION COMPLETED]");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
