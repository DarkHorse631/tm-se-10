package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "pd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Remove project--");
        System.out.print("Enter the project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getProjectService().remove(serviceLocator.getUserService().getCurrentUser().getId(), projectId);
        serviceLocator.getTaskService().deleteByProjectId(serviceLocator.getUserService().getCurrentUser().getId(), projectId);
        System.out.println("[PROJECT DELETED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
