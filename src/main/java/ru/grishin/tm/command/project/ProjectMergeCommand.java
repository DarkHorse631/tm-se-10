package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

import java.util.Date;

public final class ProjectMergeCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "pm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Merge project.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Merge project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter project name: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter project description: ");
        final String description = serviceLocator.getTerminalService().getScanner().nextLine();
        Project project = new Project();
        project.setId(projectId);
        project.setUserId(serviceLocator.getUserService().getCurrentUser().getId());
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        serviceLocator.getProjectService().merge(project);
        System.out.println("[PROJECT MERGED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
