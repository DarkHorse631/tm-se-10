package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

import java.util.Collection;

public final class ProjectFindByNameCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfn";
    }

    @Override
    public @NotNull String getDescription() {
        return "Find project by part of name.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Find project by part of name--");
        System.out.print("Enter part of name: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        final Collection<Project> projectList = serviceLocator.getProjectService().findByName(serviceLocator.getUserService().getCurrentUser().getId(), name);
        if (projectList.isEmpty()) System.out.println("[PROJECT NOT FOUND]");
        for (Project project : projectList) {
            System.out.println(project);
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
