package ru.grishin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.*;

public final class HelpCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getTerminalService().getCommands()) {
            System.out.println("[" + command.getName() + "] : " + command.getDescription());
        }
        System.out.println("[exit] : Exit form application.");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }
}
