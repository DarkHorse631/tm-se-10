package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

import java.util.List;

public final class TaskSortedViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "tsv";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task sorted by date or status.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Show sorted tasks--");
        System.out.print("Choose order: [by-create,by-start,by-finish,by-status]: ");
        final String order = serviceLocator.getTerminalService().getScanner().nextLine();
        final List<Task> taskList = serviceLocator.getTaskService().sortedByComparator(
                serviceLocator.getUserService().getCurrentUser().getId(), serviceLocator.getTaskService().getComparator(order));
        if (taskList == null) System.out.println("[TASK LIST IS EMPTY]");
        for (Task task : taskList) {
            System.out.println(task);
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
