package ru.grishin.tm.enumerate;

public enum RoleType {

    ADMIN("Administrator"),
    ANONYMOUS_USER("Anonymous"),
    USER("User");

    private final String role;

    public String getRole() {
        return role;
    }

    RoleType(String role) {
        this.role = role;
    }
}
