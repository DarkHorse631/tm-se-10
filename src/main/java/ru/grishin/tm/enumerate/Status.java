package ru.grishin.tm.enumerate;

public enum Status {
    PLANNED("planned"),
    IN_PROCESS("in process"),
    DONE("done");

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    private final String status;

}
